package com.example.lenovo.bitotsavclan;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {

    Button login,register;

  //bitotsav-67fae

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            Intent a=new Intent(MainActivity.this,UserHome.class);
            startActivity(a);
        } else {
            // No user is signed in}
            setContentView(R.layout.activity_main);


                login = (Button) findViewById(R.id.login);
                register = (Button) findViewById(R.id.register);
                login.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent obj = new Intent(MainActivity.this, login.class);
                        startActivity(obj);

                    }
                });


                register.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Toast.makeText(getBaseContext(), "Register form!" , Toast.LENGTH_SHORT ).show();
                        Intent obj = new Intent(MainActivity.this, register.class);
                        startActivity(obj);
                    }
                });
        }
    }
    @Override
    public void onBackPressed() {
        Intent a = new Intent(Intent.ACTION_MAIN);
        a.addCategory(Intent.CATEGORY_HOME);
        a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
    }
}
