package com.example.lenovo.bitotsavclan;


import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.IOException;

/**
 * Created by LENOVO on 21-Apr-19.
 */

public class fragment_profile extends Fragment implements OnBackPressed {

    ProgressDialog progressDialog;
    Bitmap bitmap;
    FirebaseStorage storage;
    StorageReference storageReference;
    String uid="";
    Dialog myDialog;
    Activity context;
    TextView name,location,education,work,gender,dob,batch,company,experience,mobile,email,designation;
    ImageView edit,logout,profilepic;
    private FirebaseDatabase database;
    private final int PICK_IMAGE_REQUEST = 71;
    static int count=0;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        context=getActivity();
        return inflater.inflate(R.layout.activity_fragment_profile, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        profilepic = (ImageView) view.findViewById(R.id.user_profilepic);
        name = (TextView) view.findViewById(R.id.name);
        location = (TextView) view.findViewById(R.id.user_location);
        education = (TextView) view.findViewById(R.id.user_education);
        work = (TextView) view.findViewById(R.id.user_work);
        gender = (TextView) view.findViewById(R.id.user_gender);
        dob = (TextView) view.findViewById(R.id.user_dob);
        batch = (TextView) view.findViewById(R.id.user_batch);
        company = (TextView) view.findViewById(R.id.user_company);
        experience = (TextView) view.findViewById(R.id.user_experience);
        mobile = (TextView) view.findViewById(R.id.user_mobile);
        email = (TextView) view.findViewById(R.id.user_email);
        designation=(TextView)view.findViewById(R.id.user_designation);
        logout=(ImageView)view.findViewById(R.id.logout);
        edit=(ImageView)view.findViewById(R.id.edit);


        myDialog  = new Dialog(context);
        profilepic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowPopup(v);
            }
        });

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 Intent ob=new Intent(context,update_profile.class);
                 startActivity(ob);
            }
        });
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 count=0;
                  FirebaseAuth.getInstance().signOut();
                  Intent intent=new Intent(context,MainActivity.class);
                  fragment_people.i=0;
                  intent.putExtra("finish", true); // if you are checking for this in your other Activities
                  intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                        Intent.FLAG_ACTIVITY_CLEAR_TASK |
                        Intent.FLAG_ACTIVITY_NEW_TASK);
                 getActivity().finish();
                 startActivity(intent);
            }
        });
        progressDialog = new ProgressDialog(context);
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        uid=user.getUid();

        if(count==0) {
            getProfilePic();
            count=1;
        }
        else if(count==2) {
            getProfilePic();
            count=1;
        }
        else{
            profilepic.setImageBitmap(bitmap);
        }

        database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference();
        ref.child("Users").child(user.getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                 User user = dataSnapshot.getValue(User.class);
                    name.setText(user.getName());
                    location.setText(user.getCity());
                    education.setText(user.getEducation());
                    work.setText(user.getWork());
                    gender.setText(user.getGender());
                    dob.setText(user.getDob());
                    email.setText(user.getEmail());
                    company.setText(user.getCompany());
                    experience.setText(user.getExperience());
                    designation.setText(user.getDesignation());

                    if(!(user.getBatch()==0))
                        batch.setText(Integer.toString(user.getBatch()));
                    else
                        batch.setText("");
                    if(!(user.getNumber()==0))
                        mobile.setText(Long.toString(user.getNumber()));
                    else
                        mobile.setText("");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });

    }


    protected void getProfilePic(){
        progressDialog.show();
        progressDialog.setMessage("Loading..");
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();
        StorageReference myRef = storageReference.child("User/profilepicture/"+ uid);
        try {
            final File localFile = File.createTempFile("images", "jpg");
            myRef.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                    bitmap = BitmapFactory.decodeFile(localFile.getAbsolutePath());
                    profilepic.setImageBitmap(bitmap);
                    progressDialog.dismiss();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    bitmap= BitmapFactory.decodeResource(getResources(),R.drawable.man);
                    profilepic.setImageResource(R.drawable.man);
                    progressDialog.dismiss();

                }
            });
        } catch (IOException e ) { }
    }


    public void ShowPopup(View v) {
        TextView view,upload,remove;
        Button btnFollow;
        myDialog.setContentView(R.layout.profile_pic_popup);
        view =(TextView) myDialog.findViewById(R.id.view_pic);
        upload =(TextView) myDialog.findViewById(R.id.upload_pic);
        remove =(TextView) myDialog.findViewById(R.id.remove_pic);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
                Intent a=new Intent(context,profilePicture.class);
                a.putExtra("uid",uid);
                startActivity(a);

            }
        });
        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
                Intent a=new Intent(context,profilepic_upload.class);
                a.putExtra("uid",uid);
                count=2;
                startActivity(a);
            }
        });
        remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Remove", Toast.LENGTH_SHORT).show();
            }
        });
        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myDialog.show();
    }



   @Override
    public boolean onBackPressed() {
        Fragment fragment = new fragment_home();
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .commit();
        return true;
    }
}
