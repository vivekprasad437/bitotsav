package com.example.lenovo.bitotsavclan;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;



/**
 * Created by LENOVO on 20-Apr-19.
 */

public class User implements Parcelable  {

    //private Bitmap bitmap;
    private String uid;
    private String name;
    private String email;
    private long number;
    private String education;
    private String gender;
    private String dob;
    private String company;
    private String experience;
    private String  designation;
    private String city;
    private String work;
    private int batch;


    public User() {
      //  this.bitmap= null;
        this.uid = "";
        this.name = "";
        this.email = "";
        this.work="";
        this.number = 0;
        this.education="";
        this.gender = "";
        this.dob = "";
        this.company = "";
        this.batch = 0;
        this.experience="";
        this.designation="";
        this.city="";
    }

    public User(String name, String company, String designation, String city) {
        this.name = name;
        this.company = company;
        this.designation = designation;
        this.city = city;
    }

    /*public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }*/

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getWork() {
        return work;
    }

    public void setWork(String work) {
        this.work = work;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getNumber() {
        return number;
    }

    public void setNumber(long number) {
        this.number = number;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public int getBatch() {
        return batch;
    }

    public void setBatch(int batch) {
        this.batch = batch;
    }

    public static Creator getCREATOR() {
        return CREATOR;
    }

    public User(Parcel in ) {
        readFromParcel( in );
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public User createFromParcel(Parcel in ) {
            return new User( in );
        }

        public User[] newArray(int size) {
            return new User[size];
        }
    };

    @Override
    public int describeContents() {

        return 0;
    }
    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(name);
        dest.writeString(email);
        dest.writeLong(number);
        dest.writeString(experience);
        dest.writeString(gender);
        dest.writeString(dob);
        dest.writeString(city);
        dest.writeString(company);
        dest.writeInt(batch);
        dest.writeString(work);
        dest.writeString(education);
        dest.writeString(designation);
    }

    private void readFromParcel(Parcel in ) {

        name = in .readString();
        email = in .readString();
        number= in.readLong();
        experience   = in .readString();
        gender = in .readString();
        dob = in.readString();
        city = in.readString();
        company = in.readString();
        batch = in.readInt();
        work=in.readString();
        education=in.readString();
        designation=in.readString();
    }


}
