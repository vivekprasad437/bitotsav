package com.example.lenovo.bitotsavclan;


import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.IOException;

/**
 * Created by VIVEK on 27-Apr-19.
 */

public class profilePicture extends AppCompatActivity {

    FirebaseStorage storage;
    StorageReference storageReference;
    Bitmap bitmap;
    ImageView iv;
    String uid="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_profilepic);
        iv=(ImageView)findViewById(R.id.imgView);

        final ProgressDialog progressDialog = new ProgressDialog(this);

        progressDialog.show();
        progressDialog.setMessage("Loading... ");
        Intent intent=getIntent();
        uid=intent.getStringExtra("uid");

        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();
        StorageReference myRef = storageReference.child("User/profilepicture/"+ uid);

        try {
            final File localFile = File.createTempFile("images", "jpg");
            myRef.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                    bitmap = BitmapFactory.decodeFile(localFile.getAbsolutePath());
                    iv.setImageBitmap(bitmap);
                    progressDialog.dismiss();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                }
            });
        } catch (IOException e) { }
    }
}
