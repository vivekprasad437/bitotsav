package com.example.lenovo.bitotsavclan;



import android.app.Activity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;


import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import java.util.ArrayList;


/**
 * Created by LENOVO on 21-Apr-19.
 */

public class fragment_people extends Fragment implements OnBackPressed{

    private Object lock= new Object();
    FirebaseStorage storage;
    StorageReference storageReference;
    Bitmap bitmap,bitmap1;
    static int i=0;
    ArrayList<User> value=new ArrayList<User>();
    Activity context;
    ShimmerFrameLayout container;
    ListView list;
    CustomAdapter customAdapter;
    SearchView sv;
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        container = (ShimmerFrameLayout) view.findViewById(R.id.shimmer_view_container);
        list = (ListView) view.findViewById(R.id.listView);
        sv=(SearchView)view.findViewById(R.id.searchView);

        System.out.println("i="+i);

        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                if(!s.isEmpty()) {
                    customAdapter.getFilter().filter(s);
                }
                    return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                if(!s.equals(""))
                customAdapter.getFilter().filter(s);
                else if(s.equals("")&& i>=1)
                    customAdapter.getFilter().filter(s);
                return false;
            }
        });



        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                    Fragment fragment = new fragment_otherprofile();
                    Bundle args = new Bundle();
                    User ob = (User) parent.getItemAtPosition(position);
                    String uid=ob.getUid();
                    args.putString("UID", uid);
                    fragment.setArguments(args);

                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.fragment_container, fragment)
                            .addToBackStack(null)
                            .commit();

            }
        });

        if(i<1) {
            container.startShimmer();
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
            ref.child("Users").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    storeData(dataSnapshot);
                    i++;
                }
                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    System.out.println("The read failed: " + databaseError.getCode());
                }
            });
        }
        else{
            PrintData();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        context=getActivity();
        return inflater.inflate(R.layout.activity_fragment_people, null);
    }

    private void storeData(DataSnapshot dataSnapshot) {
        for (DataSnapshot unit : dataSnapshot.getChildren()) {
            User ob = unit.getValue(User.class);
            if(i>=1) {
                break;
            }
            value.add(ob);
        }
        customAdapter = new CustomAdapter(context, value);
        PrintData();

    }
   public void PrintData(){

       String size=Integer.toString(value.size());
        list.setAdapter(customAdapter);
        container.setVisibility(View.GONE);
        container.stopShimmer();
   }

    public  void setHomeItem(Activity activity) {

        BottomNavigationView bottomNavigationView = (BottomNavigationView) getActivity().findViewById(R.id.bottom_navigation);
        int seletedItemId = bottomNavigationView.getSelectedItemId();
        if (R.id.navigation_home != seletedItemId) {
            bottomNavigationView.setSelectedItemId(R.id.navigation_home);
        }
    }


        @Override
        public boolean onBackPressed() {
        setHomeItem(context);
        Fragment fragment = new fragment_home();
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .commit();

          return true;
       }
}
