package com.example.lenovo.bitotsavclan;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.IOException;

/**
 * Created by LENOVO on 23-Apr-19.
 */

public class fragment_otherprofile extends Fragment implements OnBackPressed  {


    public fragment_otherprofile() {
    }

    ProgressDialog progressDialog;
    FirebaseStorage storage;
    StorageReference storageReference;
    Bitmap bitmap;
    String uid="";
    Activity context;
    TextView name,location,education,work,gender,dob,batch,company,experience,mobile,email,designation;
    ImageView proPic;
    private FirebaseDatabase database;
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
         uid = getArguments().getString("UID");

        super.onViewCreated(view, savedInstanceState);
        name = (TextView) view.findViewById(R.id.name);
        location = (TextView) view.findViewById(R.id.user_location);
        education = (TextView) view.findViewById(R.id.user_education);
        work = (TextView) view.findViewById(R.id.user_work);
        gender = (TextView) view.findViewById(R.id.user_gender);
        dob = (TextView) view.findViewById(R.id.user_dob);
        batch = (TextView) view.findViewById(R.id.user_batch);
        company = (TextView) view.findViewById(R.id.user_company);
        experience = (TextView) view.findViewById(R.id.user_experience);
        mobile = (TextView) view.findViewById(R.id.user_mobile);
        email = (TextView) view.findViewById(R.id.user_email);
        designation=(TextView)view.findViewById(R.id.user_designation);
        proPic=(ImageView)view.findViewById(R.id.profile_picture);

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        progressDialog = new ProgressDialog(context);
        progressDialog.show();
        progressDialog.setMessage("Loading..");
        getProfilePic();

        database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference();
        ref.child("Users").child(uid).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);

                name.setText(user.getName());
                location.setText(user.getCity());
                education.setText(user.getEducation());
                work.setText(user.getWork());
                gender.setText(user.getGender());
                dob.setText(user.getDob());
                email.setText(user.getEmail());
                company.setText(user.getCompany());
                experience.setText(user.getExperience());
                designation.setText(user.getDesignation());

                if(!(user.getBatch()==0))
                    batch.setText(Integer.toString(user.getBatch()));
                else
                    batch.setText("");
                if(!(user.getNumber()==0))
                    mobile.setText(Long.toString(user.getNumber()));
                else
                    mobile.setText("");


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });

    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //with the fragment you want to inflate
        //if it is DashboardFragment it should have R.layout.fragment_dashboard
        context=getActivity();
        return inflater.inflate(R.layout.activity_otherprofile, null);

    }

    protected void getProfilePic(){
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();
        StorageReference myRef = storageReference.child("User/profilepicture/"+ uid);

        try {
            final File localFile = File.createTempFile("images", "jpg");
            myRef.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                    bitmap = BitmapFactory.decodeFile(localFile.getAbsolutePath());
                    proPic.setImageBitmap(bitmap);
                    progressDialog.dismiss();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    progressDialog.dismiss();
                }
            });
        } catch (IOException e ) { }
    }

    public  void setHomeItem(Activity activity) {

        BottomNavigationView bottomNavigationView = (BottomNavigationView) getActivity().findViewById(R.id.bottom_navigation);
        int seletedItemId = bottomNavigationView.getSelectedItemId();

        if (R.id.navigation_people != seletedItemId) {
            bottomNavigationView.setSelectedItemId(R.id.navigation_people);
        }


    }
    @Override
    public boolean onBackPressed() {

        /*Fragment fragment = new fragment_people();
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .commit();*/
        setHomeItem(context);
        getFragmentManager().popBackStack();
        return true;
    }

}


