package com.example.lenovo.bitotsavclan;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class login extends AppCompatActivity {
   Button login,register,forgot;
   EditText ed1,ed2;
   String mail="",pass="";
   private FirebaseAuth firebaseauth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        login=(Button)findViewById(R.id.login);
        register=(Button)findViewById(R.id.register);
        forgot=(Button)findViewById(R.id.forgot);

        ed1=(EditText)findViewById(R.id.mail);
        ed2=(EditText)findViewById(R.id.pass);



        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mail = ed1.getText().toString();
                pass = ed2.getText().toString();
                if (mail.equals("") || pass.equals("")) {
                    Toast.makeText(getBaseContext(), "Fill all the details!!" , Toast.LENGTH_SHORT ).show();
                }
                else{
                    firebaseauth = FirebaseAuth.getInstance();
                    firebaseauth.signInWithEmailAndPassword(mail, pass)
                            .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        Toast.makeText(getBaseContext(), "Successful!", Toast.LENGTH_SHORT).show();
                                        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                                        Intent intent = new Intent(login.this,UserHome.class);
                                         startActivity(intent);
                                    } else {
                                        Toast.makeText(getBaseContext(), "Failed!", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                  }
            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               // Toast.makeText(getBaseContext(), "Register!" , Toast.LENGTH_SHORT ).show();
                Intent obj =new Intent(login.this,register.class);
                startActivity(obj);
            }
        });
        forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }
    @Override
    public void onBackPressed() {
        Intent ob=new Intent(login.this,MainActivity.class);
        startActivity(ob);
        finish();
    }
}
