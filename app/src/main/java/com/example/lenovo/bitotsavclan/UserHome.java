package com.example.lenovo.bitotsavclan;



import android.app.Activity;
import android.app.FragmentManager;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;


import com.google.firebase.database.FirebaseDatabase;

public class UserHome extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener{


    private ActionBar toolbar;
    TextView tv1,tv2,tv3;
    private FirebaseDatabase database;
    Fragment frags[]=new Fragment[3];
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_home);

      frags[0]=new fragment_home();
      frags[1]=new fragment_people();
      frags[2]=new fragment_profile();
      loadFragment(0);
       BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                Fragment fragment=null;
                Fragment fragment1=null;
                switch (item.getItemId()) {
                    case R.id.navigation_home:
                        loadFragment(0);
                        break;
                    case R.id.navigation_people:
                        loadFragment(1);
                         break;
                    case R.id.navigation_profile:
                        loadFragment(2);
                        break;
                }
                return true;
            }
        });
    }

    public boolean loadFragment(int fragNo) {
        //switching fragment

        Fragment f1=getSupportFragmentManager().findFragmentByTag("HOME");
        Fragment f2=getSupportFragmentManager().findFragmentByTag("PEOPLE");
        Fragment f3=getSupportFragmentManager().findFragmentByTag("PROFILE");

        if (fragNo >=0 && fragNo<=2) {

            String s="";
            if(fragNo==0)
                s="HOME";
            else if(fragNo==1)
                s="PEOPLE";
            else
                s="PROFILE";
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragment_container,frags[fragNo] ,s)
                        .commit();
                if(f1!=null)
                    frags[0]=f1;
                if(f2!=null)
                    frags[1]=f2;
                if(f3!=null)
                    frags[2]=f3;
            return true;
        }
        return false;
    }
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        return true;
    }

    public static void setHomeItem(Activity activity) {

        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                activity.findViewById(R.id.bottom_navigation);
        bottomNavigationView.setSelectedItemId(R.id.navigation_home);
    }


    @Override
    public void onBackPressed() {
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        int seletedItemId = bottomNavigationView.getSelectedItemId();

        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (!(fragment instanceof OnBackPressed) || !((OnBackPressed) fragment).onBackPressed()) {
            if (R.id.navigation_home != seletedItemId) {
                setHomeItem(UserHome.this);
            }
            super.onBackPressed();
        }
    }
}
