package com.example.lenovo.bitotsavclan;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


/**
 * Created by LENOVO on 21-Apr-19.
 */
public class fragment_home extends Fragment implements OnBackPressed{

    Activity context;
    private LinearLayout mGallery;
    private int[] mImgIds;
    private LayoutInflater mInflater;


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mInflater = LayoutInflater.from(context);
        initData(view);
        initView(view);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //with the fragment you want to inflate
        //like if the class is HomeFragment it should have R.layout.home_fragment
        context=getActivity();
        return inflater.inflate(R.layout.activity_fragment_home, null);
    }

    private void initData(View view)
    {
        mImgIds = new int[] { R.drawable.bitlogo,R.drawable.campus,R.drawable.library, R.drawable.class_room,R.drawable.computer_lab};
    }

    private void initView(View view1)
    {
        mGallery = (LinearLayout) view1.findViewById(R.id.id_gallery);

        for (int i = 0; i < mImgIds.length; i++)
        {

            View view = mInflater.inflate(R.layout.home_image, mGallery, false);
            ImageView img = (ImageView) view.findViewById(R.id.id_index_gallery_item_image);
            img.setImageResource(mImgIds[i]);
            TextView txt = (TextView) view
                    .findViewById(R.id.id_index_gallery_item_text);
            txt.setText("info "+i);
            mGallery.addView(view);
        }
    }
   @Override
    public boolean onBackPressed() {

        Intent a = new Intent(Intent.ACTION_MAIN);
        a.addCategory(Intent.CATEGORY_HOME);
        a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
        return true;
    }


}
