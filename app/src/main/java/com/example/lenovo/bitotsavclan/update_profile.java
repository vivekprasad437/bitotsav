package com.example.lenovo.bitotsavclan;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class update_profile extends AppCompatActivity {

    EditText name,education,work,gender,dob,batch,company,experience,mobile,email,city,designation;
    String uid="";
    private static FirebaseDatabase database;
    Button cancel,update;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile);

        name=(EditText)findViewById(R.id.edit_name);
        education = (EditText) findViewById(R.id.edit_education);
        work = (EditText) findViewById(R.id.edit_work);
        gender = (EditText) findViewById(R.id.edit_gender);
        dob = (EditText) findViewById(R.id.edit_dob);
        batch = (EditText) findViewById(R.id.edit_batch);
        company = (EditText) findViewById(R.id.edit_company);
        experience = (EditText)findViewById(R.id.edit_experience);
        mobile = (EditText) findViewById(R.id.edit_number);
        email = (EditText) findViewById(R.id.edit_email);
        city=(EditText)findViewById(R.id.edit_city);
        designation=(EditText)findViewById(R.id.edit_designation);

        cancel=(Button)findViewById(R.id.cancel);
        update=(Button)findViewById(R.id.update);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 update();
            }
        });

        fetch();
    }
    public void fetch(){

        FirebaseUser fb_user = FirebaseAuth.getInstance().getCurrentUser();
        database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference();
        uid=fb_user.getUid().toString();
        ref.child("Users").child(fb_user.getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);

                    if(!user.getName().equals(""))
                         name.setText(user.getName());
                    if(!user.getEducation().equals(""))
                        education.setText(user.getEducation());
                    if(!user.getWork().equals(""))
                        work.setText(user.getWork());
                    if(!user.getGender().equals(""))
                        gender.setText(user.getGender());
                    if(!user.getDob().equals(""))
                        dob.setText(user.getDob());
                    if(!(user.getBatch()==0))
                        batch.setText(Integer.toString(user.getBatch()));
                    if(!user.getCompany().equals(""))
                        company.setText(user.getCompany());
                    if(!user.getExperience().equals(""))
                        experience.setText(user.getExperience());
                    if(!(user.getNumber()==0))
                        mobile.setText(Long.toString(user.getNumber()));
                    if(!user.getEmail().equals(""))
                        email.setText(user.getEmail());
                    if(!user.getCity().equals(""))
                        city.setText(user.getCity());
                    if(!user.getDesignation().equals(""))
                       designation.setText(user.getDesignation());

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });
    }


    public  void update(){
        User ob=new User();
        String name="",education="",work="",gender="",dob="",company="",experience="",email="",city="",designation="";
        String batch="", mobile="";
        name=(this.name).getText().toString().trim();
        education=(this.education).getText().toString().trim();
        work=(this.work).getText().toString().trim();
        gender=(this.gender).getText().toString().trim();
        dob=(this.dob).getText().toString().trim();
        batch=(this.batch).getText().toString().trim();
        company=(this.company).getText().toString().trim();
        experience=(this.experience).getText().toString().trim();
        mobile=(this.mobile).getText().toString().trim();
        email=(this.email).getText().toString().trim();
        city=(this.city).getText().toString().trim();
        designation=(this.designation).getText().toString().trim();

        if(!name.equals(""))
            ob.setName(name);
        if(!education.equals(""))
            ob.setEducation(education);
        if(!work.equals(""))
            ob.setWork(work);
        if(!gender.equals(""))
            ob.setGender(gender);
        if(!dob.equals(""))
            ob.setDob(dob);
        if(!batch.equals(""))
            ob.setBatch(Integer.parseInt(batch));
        if(!company.equals(""))
            ob.setCompany(company);
        if(!experience.equals(""))
            ob.setExperience(experience);
        if(!mobile.equals(""))
            ob.setNumber(Long.parseLong(mobile));
        if(!email.equals(""))
            ob.setEmail(email);
        if(!city.equals(""))
            ob.setCity(city);
        if(!designation.equals(""))
            ob.setDesignation(designation);

        ob.setUid(uid);
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference();
        myRef.child("Users").child(user.getUid()).setValue(ob);
        Toast.makeText(getBaseContext(), "Updated!", Toast.LENGTH_SHORT).show();
        finish();

    }
}
