package com.example.lenovo.bitotsavclan;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageException;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by LENOVO on 26-Apr-19.
 */

public class CustomAdapter extends BaseAdapter  implements Filterable{


    FirebaseStorage storage;
    StorageReference storageReference;
    private  Bitmap bitmap;
    private ArrayList<User> users;
    private LayoutInflater inflter;
    private ArrayList<User> mySearchFilter;
    private ValueFilter valueFilter;
    private String uid="";
    private Context context;
    public CustomAdapter(Context context,ArrayList<User> users){
        this.context=context;
        this.users=users;
        mySearchFilter=users;
        inflter = (LayoutInflater.from(context));
    }
    @Override
    public int getCount() {
      return users.size();
    }

    @Override
    public User getItem(int i) {
        return users.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        if(view==null) {
            view = inflter.inflate(R.layout.activity_data_holder, null);
        }
          final ImageView proPic=(ImageView)view.findViewById(R.id.user_pic);
          TextView name = (TextView)view.findViewById(R.id.user_name);
          TextView company = (TextView)view.findViewById(R.id.user_company);
          TextView designation = (TextView)view.findViewById(R.id.user_designation);
          TextView city = (TextView)view.findViewById(R.id.user_city);
          uid=users.get(i).getUid();
           storage = FirebaseStorage.getInstance();
           storageReference = storage.getReference();
           storageReference.child("User/profilepicture/"+ uid).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                Picasso.with(context).load(uri).fit().centerCrop()
                        .error(R.drawable.man)
                        .into(proPic);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Picasso.with(context).load(R.drawable.man)
                        .into(proPic);
            }
        });
          view.setTag(users.get(i).getUid());
          name.setText(users.get(i).getName());
          company.setText(users.get(i).getCompany());
          designation.setText(users.get(i).getDesignation());
          city.setText(users.get(i).getCity());
          return view;
    }




    @Override
    public Filter  getFilter() {
        if (valueFilter == null) {
            valueFilter = new ValueFilter();
        }
        return valueFilter;
    }
    private class ValueFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            FilterResults results = new FilterResults();

            if (constraint != null && constraint.length() > 0 ) {
                ArrayList<User> filterList = new ArrayList<User>(mySearchFilter.size());

                int count=0;
                for (int i = 0; i < mySearchFilter.size(); i++) {
                    if ((mySearchFilter.get(i).getName().toUpperCase())
                            .contains(constraint.toString().toUpperCase())
                            || (mySearchFilter.get(i).getDesignation().toUpperCase())
                            .contains(constraint.toString().toUpperCase())
                            ||(mySearchFilter.get(i).getCity().toUpperCase())
                            .contains(constraint.toString().toUpperCase())
                            ||(Integer.toString(mySearchFilter.get(i).getBatch()))
                            .contains(constraint.toString().toUpperCase())
                            ||(mySearchFilter.get(i).getCompany().toUpperCase())
                            .contains(constraint.toString().toUpperCase())) {

                        User mUser = mySearchFilter.get(i);
                       filterList.add(mUser);
                       count++;
                    }
                }
                results.count = count;
                results.values = filterList;
            }
            else {
                results.count = mySearchFilter.size();
                results.values = mySearchFilter;
            }
            if(constraint.toString().equals(" ")){
                results.count = mySearchFilter.size();
                results.values = mySearchFilter;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            users=(ArrayList<User>) results.values;
            notifyDataSetChanged();
        }
    }
}
