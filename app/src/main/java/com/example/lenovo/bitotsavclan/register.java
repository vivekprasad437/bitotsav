package com.example.lenovo.bitotsavclan;

import android.content.Intent;
import android.content.SyncStatusObserver;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class register extends AppCompatActivity {

    EditText ed1,ed3,ed4,ed5;
    RadioGroup rb;
    Button sub;
    private FirebaseDatabase database;
    private FirebaseAuth firebaseAuth;
    User ob;
    String password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ed1=(EditText)findViewById(R.id.name);
        ed3=(EditText)findViewById(R.id.mail);
        ed4=(EditText)findViewById(R.id.password);
        ed5=(EditText)findViewById(R.id.confirmpassword);
        rb=(RadioGroup)findViewById(R.id.gender);
        sub=(Button)findViewById(R.id.register);


        sub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ob=new User();
                String name=ed1.getText().toString().trim();
                String mail=ed3.getText().toString().trim();
                String pass=ed4.getText().toString().trim();
                String conPass=(ed5.getText().toString());

                if(name.equals("")|| mail.equals("") || conPass.equals("")|| pass.equals("")) {
                    Toast.makeText(getBaseContext(), "Fill all the details!!" , Toast.LENGTH_SHORT ).show();
                }else if(!conPass.equals(pass)){
                    Toast.makeText(getBaseContext(), "Passwords do not match!!" , Toast.LENGTH_SHORT ).show();
                }
                else {
                    ob.setName(name);
                    ob.setEmail(mail);
                    int selectedId = rb.getCheckedRadioButtonId();
                    RadioButton rb1 = (RadioButton) findViewById(selectedId);
                    ob.setGender(rb1.getText().toString());
                    password = ed5.getText().toString();
                    registerUser();
                }

            }
        });

    }

    private void registerUser(){
        firebaseAuth = FirebaseAuth.getInstance();

        //creating a new user

        firebaseAuth.createUserWithEmailAndPassword(ob.getEmail(), password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>()
                {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        //checking if success
                        if(task.isSuccessful()){

                            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                            UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                                    .setDisplayName(ob.getName()).build();
                            user.updateProfile(profileUpdates);
                            database = FirebaseDatabase.getInstance();
                            DatabaseReference myRef = database.getReference();
                            ob.setUid(user.getUid());
                            myRef.child("Users").child(user.getUid()).setValue(ob);
                            Toast.makeText(getBaseContext(), "Registration Successful!!!" , Toast.LENGTH_SHORT ).show();
                            Intent intent = new Intent(register.this, login.class);
                            startActivity(intent);
                            finish();


                           // Intent intent = new Intent(register.this,Home.class);
                           // intent.putExtra("parcel_data",ob);
                          //  startActivity(intent);
                            //myRef.child("Users").push().setValue(ob);

                        }else{
                           Toast.makeText(getBaseContext(), "Email already registered!!" , Toast.LENGTH_SHORT ).show();
                        }

                    }
                });

    }
    @Override
    public void onBackPressed() {
        finish();
    }
}
